import { getHeroById } from './bases/08-imp-exp';


// console.log('Init');

// // resolve: Si todo sale Bien
// // reject: Si la promesa retorna algun error
// new Promise((resolve, reject) => {
//     resolve('Promise resolved');
// })
// .then(console.log)
// .catch(console.error);

// console.log('End');

const getHeroByIdAsync = (heroId) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const hero = getHeroById(heroId);
            hero ? resolve(hero.name) : reject('Hero doesn\'t exist');
        }, 1000);
    });
};

getHeroByIdAsync(2)
    .then(hero => console.log(`Hero: ${hero}`))
    .catch(console.error)


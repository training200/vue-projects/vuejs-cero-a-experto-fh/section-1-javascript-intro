
const person = {
    name: 'Tony',
    lastName: 'Stark',
    age: 45,
    address: {
        city: 'Winterfall',
        zip: 532163,
        latitude: 14.2323,
        longitude: 34.63263
    }
}

const person2 = { ...person };

person2.name = 'Bran';

console.log('Person: ', person);
console.log('Person2: ', person2);
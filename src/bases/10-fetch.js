
const apiKey = 'em3iPLjHL6JSD8t2r1tYWOQPMYbLYyQI';

// api.giphy.com/v1/gifs/random

fetch(`https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`)
    .then(response => response.json())
    .then(({data}) => {
        const { url } = data.images.original;
        
        const image = document.createElement('img');
        image.src = url;

        document.body.append(image);


    })
    .catch(console.error);

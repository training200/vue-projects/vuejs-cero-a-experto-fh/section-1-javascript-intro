
const heroe = {
    id: 123123123,
    name: 'Tony',
    age: 45,
    codeName: 'Iron-Man'
};

const createHero = ({ id, name, age, codeName, power = 'Doesn\'t have a power' }) => (
    {
        id,
        name,
        codeName,
        age,
        power
    });

console.log('Heroe: ', createHero(heroe));


const heroes = [
    {
        id: 1,
        name: 'Batman'
    },
    {
        id: 2,
        name: 'Superman'
    }
];

const isExists = (heroeId) => heroes.some(heroe => heroe.id === heroeId);

const searchId = 2;

console.log(`Heroe with id ${searchId} exist? `, isExists(searchId));
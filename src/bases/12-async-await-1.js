
const myPromise = () => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('We have a value in the promise');
        }, 1000);
    });
};

const measureTimeAsync = async () => {

    try {
        console.log('Init');

        const response = await myPromise();
    
        console.log(response);

        console.log('End');
    
        return 'End to measure time async';        
    } catch (error) {
        throw 'Error in measureTimeAsync';    
    }

    
};

measureTimeAsync()
    .then(console.log)
    .catch(console.error);





import { heroes } from '../data/heroes';

export const getHeroById = (heroId) => heroes.find(heroe => heroe.id === heroId);

export const getHeroesByOwner = (owner) => heroes.filter(heroe => heroe.owner === owner);

// console.log('getHeroById', getHeroById(1));

// console.log('getHeroesByOwner', getHeroesByOwner('DC'));

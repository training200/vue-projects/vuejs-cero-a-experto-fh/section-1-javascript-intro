
const array = [1, 2, 3, 4];

array.push(5);

const array2 = [...array];
array2.push(6);

const array3 = array2.map(number => number * 2);

console.log('Array', array);
console.log('Array2', array2);
console.log('Array3', array3);

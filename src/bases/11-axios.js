import axios from 'axios';

const apiKey = 'em3iPLjHL6JSD8t2r1tYWOQPMYbLYyQI';

// `https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`

const giphyApi = axios.create({
    baseURL: 'https://api.giphy.com/v1/gifs',
    params: {
        api_key: apiKey
    }
});

// giphyApi.get('/random')
//     .then(({ data }) => {
//         const { url } = data.data.images.original;

//         const image = document.createElement('img');
//         image.src = url;

//         document.body.append(image);

//     });

export default giphyApi;